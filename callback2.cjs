let lists=require('./lists.json');

function callback2(id,cb){
    setTimeout(() => {
        if(lists[id]!=undefined){
            return cb(null,lists[id]);
        }else{
            return cb( Error({name : "not found"}));
        }
    }, 3*1000);
}

module.exports=callback2;