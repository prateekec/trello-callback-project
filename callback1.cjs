const boards=require('./boards.json');
function callback1(boardId,cb){
    setTimeout(() => {
        for(let idx in boards){
            if(boards[idx].id==boardId){
                return cb(null,boards[idx]);
            }
        }
        return cb( Error({name : "not found"}));
    }, 3*1000);
}

module.exports=callback1;