let cards=require('./cards.json');

function callback3(id,cb){
    setTimeout(() => {
        if(cards[id]!=undefined){
            return cb(null,cards[id]);
        }else{
            return cb( Error({name : "not found"}));
        }
    }, 3*1000);
}

module.exports=callback3;